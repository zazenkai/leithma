<?php

add_action( 'wp_enqueue_scripts', 'enqueue_parent_styles' );
function enqueue_parent_styles() {
   //wp_enqueue_style('bootstrap-4', 'https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css', array(), null);
   wp_enqueue_style( 'parent-style', get_template_directory_uri().'/style.css' );
   wp_enqueue_style( 'custom-sheet', get_stylesheet_directory_uri() . '/style.css', array(), filemtime( get_stylesheet_directory() . '/style.css' ));
   //wp_enqueue_script('custom-js', get_stylesheet_directory_uri() . '/js/custom.js', array(), filemtime( get_stylesheet_directory() . '/js/custom.js' ));
   wp_enqueue_style( 'custom-style-sheet', get_stylesheet_directory_uri() . '/custom.css', array(), filemtime( get_stylesheet_directory() . '/style.css' ));
   wp_enqueue_script('custom-js', get_stylesheet_directory_uri() . '/js/custom.js');
}

function add_style_attributes( $html, $handle ) {

    if ( 'bootstrap-4' === $handle ) {
        return str_replace( "media='all'", "media='all' integrity='sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2' crossorigin='anonymous'", $html );
    }

    return $html;
}
add_filter( 'style_loader_tag', 'add_style_attributes', 10, 2 );

wp_enqueue_script('script', get_stylesheet_directory_uri() . '/js/tracking.js', array(), 1.0, true);


add_action('wp_head', 'add_googleanalytics');

function add_googleanalytics() { ?>

    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-38297898-1', 'auto');
        ga('send', 'pageview');

    </script>

    <script type="text/javascript">
        (function(a,e,c,f,g,h,b,d){var k={ak:"994310963",cl:"qTzECIXJ3WoQs_aP2gM",autoreplace:"01306646890"};a[c]=a[c]||function(){(a[c].q=a[c].q||[]).push(arguments)};a[g]||(a[g]=k.ak);b=e.createElement(h);b.async=1;b.src="//www.gstatic.com/wcm/loader.js";d=e.getElementsByTagName(h)[0];d.parentNode.insertBefore(b,d);a[f]=function(b,d,e){a[c](2,b,k,d,null,new Date,e)};a[f]()})(window,document,"_googWcmImpl","_googWcmGet","_googWcmAk","script");
    </script>

    <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion_async.js"></script>

<?php }
