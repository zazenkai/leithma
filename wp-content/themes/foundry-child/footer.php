<?php 
	/**
	 * First, we need to check if we're going to override the header layout (with post meta)
	 * Or if we're going to display the global choice from the theme options.
	 * This is what ebor_get_header_layout is in charge of.
	 * 
	 * Oh yeah, exactly the same for the footer as well.
	 */
	get_template_part('inc/content-footer', ebor_get_footer_layout()); 
?>	

</div><!--/body-wrapper-->
<!-- The modal -->
<div class="modal right fade in" id="myModal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
	<div class="modal-dialog modal-side modal-bottom-right modal-dialog-centered" role="document" class="pum-content popmake-content">
		<div class="modal-content">
			<div class="modal-header">
				<button id="myModalClose" type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="modalLabel">Download your free guide!</h4>
			</div>
			<div class="modal-body">
				<div class="pop-up">
					<span class="headerblue" style="font-size:18px; margin-bottom: 1rem; display: block;" color="#0b7695">Buying or selling a company? fill in your email address so we can send you our free guide.</span>
					<form class="js-cm-form" id="subForm" action="https://www.createsend.com/t/subscribeerror?description=" method="post" data-id="A61C50BEC994754B1D79C5819EC1255CBC1F5BADF40B9E14D065AEE3865ECE0D48700BEA2A1BEA52C60886DC1D3BC9D66F8DEBCA7EB247308CFB1F078AB50B46">
						<div>
							<div>
								<label>Name </label><input aria-label="Name" id="fieldName" maxlength="200" name="cm-name" type="text">
							</div>
							<div>
								<label>Email </label><input autocomplete="Email" aria-label="Email" class="js-cm-email-input qa-input-email" id="fieldEmail" maxlength="200" name="cm-jiydiud-jiydiud" required="" type="email">
							</div>
							<div>
								<div>
									<div>
										<div>
											<input aria-required="" id="cm-privacy-consent" name="cm-privacy-consent" required="" type="checkbox">
											<label for="cm-privacy-consent">We need the contact information you provide to us to contact you about our products and services. You may unsubscribe from these communications at any time. For information on how to unsubscribe, as well as our privacy practices and commitment</label>
										</div>
										<input id="cm-privacy-consent-hidden" name="cm-privacy-consent-hidden" type="hidden" value="true">
									</div>
								</div>
								<p>
									<a href="https://leithma.co.uk/privacy-policy/" rel="noopener" target="_blank">View our privacy policy</a>
								</p>
							</div>
						</div>
						<button type="submit">Free Download</button>
					</form>
				</div>
				<script type="text/javascript" src="https://js.createsend1.com/javascript/copypastesubscribeformlogic.js"></script>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<?php 
	get_template_part('inc/content-footer','modal');
	
	global $foundry_modal_content;
	echo do_shortcode($foundry_modal_content);
	
	wp_footer(); 
?>

<!-- Initialize Bootstrap functionality -->
<script>
// Initialize tooltip component
(function($) {
	$(function () {
	  $('[data-toggle="tooltip"]').tooltip()
	})

	// Initialize popover component
	$(function () {
	  $('[data-toggle="popover"]').popover()
	})
	
    $(window).on('load', function() {
        $('#flipFlop').modal('show');
    });

	$(document).ready(function () {
    //if cookie hasn't been set...
    if (document.cookie.indexOf("ModalShown=true")<0) {
		setTimeout(function(){
        	$("#myModal").modal("show");
		}, 5000);
        //Modal has been shown, now set a cookie so it never comes back
        $("#myModalClose").click(function () {
            $("#myModal").modal("hide");
        });
        document.cookie = "ModalShown=true; expires=Fri, 31 Dec 9999 23:59:59 GMT; path=/";
    }
});
	
}(jQuery));
</script>
</body>
</html>